# Version locale du logiciel blockscad

[Blockscad](https://www.blockscad3d.com/editor/?lang=fr#) est une interface web qui propose une interface web à la manière de [scratch](https://scratch.mit.edu/about) pour la réalisation d'objets 3d. Il s'agit d'une surcouche à [openscad](https://www.openscad.org/) et le rend donc accessible au jeune public.

Ce dépôt permet de déployer une instance locale (serveur web) accessible à l'adresse http://127.0.0.1:8206

Il faut avoir [docker](https://fr.wikipedia.org/wiki/Docker_(logiciel)) installé sur son ordinateur et [git](https://fr.wikipedia.org/wiki/Git).

## Installation

### cloner ce projet

`git clone https://framagit.org/fobrice/docker-blockscad.git`

### Construire l'image docker

`docker build . -t blockscad`

### Lancer l'application

`docker run -p 8206:80 -e PORT=8206 blockscad`

### Se connecter à l'application

L'application est accessible dans votre navigateur à l'adresse http://127.0.0.1:8206