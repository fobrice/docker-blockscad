import http.server
import socketserver
import os

PORT = os.environ.get('PORT',80)

Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", int(PORT))
    httpd.serve_forever()